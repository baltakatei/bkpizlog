# bkpilog

## Summary

This git repository stores software to setup a Raspberry Pi Zero W to log the following types of data:

1. Location
2. Air pressure

## Technical Details

1. Location
  - Datum: WGS84
  - Format: NME
  - Source: BerryGPS-IMU v3.5, CAM-M8
2. Air pressure
  - Units: Pascals (Pa)
  - Source: BerryGPS-IMU v3.5, BMP388 pressure sensor
